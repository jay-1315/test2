const router = require('koa-router')()

const { putData , doneVaccines } = require('../controller/index')
const {checkvalidations , slotcheck, checkalreadyBookSlot } = require('../validator/index')

router.put('/citizen/bookslot' , checkvalidations, slotcheck, checkalreadyBookSlot, putData )
router.put('/citizen/donevaccine' , checkvalidations , checkalreadyBookSlot, doneVaccines)

module.exports = router