const Myconnection = Database.collection('hospital');

const findhospitals = (hno) => Myconnection.findOne({ hospitalCode : hno })

const totalVacsinStorageslot = (hno,vno) => Myconnection.findOne({ hospitalCode : hno, "totalVacsinStorage.code" : vno })

const availableslot = (hno,vno) => Myconnection.findOne({hospitalCode : hno , 'bookedSlot.code' : vno })

const addbookslot = (hno, vno) => Myconnection.updateOne({ hospitalCode : hno , 'bookedSlot.code' : vno },
   { $inc : {"bookedSlot.$.count" : 1 } }  )  

const addnewslot = (hno, vno) => Myconnection.updateOne({hospitalCode : hno},
     {$push : { "bookedSlot" : {
        code : vno,
        count : 1
     }}})

const removecount = (hno, vno) => Myconnection.updateOne({ hospitalCode : hno ,  'bookedSlot.code' : vno },
{ $inc : {"bookedSlot.$.count" : -1 } } )

const removetotalVacsincode = (hno,vno) => Myconnection.updateOne({ hospitalCode : hno , 'totalVacsinStorage.code' : vno },
{ $inc : {"totalVacsinStorage.$.count" : -1 } } )

module.exports = {findhospitals ,totalVacsinStorageslot ,addbookslot , addnewslot , availableslot , removecount , removetotalVacsincode}